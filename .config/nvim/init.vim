" _   _         __     ___
"| \ | | ___  __\ \   / (_)_ __ ___
"|  \| |/ _ \/ _ \ \ / /| | '_ ` _ \
"| |\  |  __/ (_) \ V / | | | | | | |
"|_| \_|\___|\___/ \_/  |_|_| |_| |_|
"

" Some basics:
	set nocompatible
	filetype plugin indent on
	set encoding=utf-8
	set number
	set relativenumber
	colorscheme darkblue
	set laststatus=2
	let anyfold_activate=1
	set foldlevel=0
	filetype off
	set path+=**
	set wildmenu
	set noshowmode
	command! MakeTags !ctags -R .
	syntax on
	let b:did_indent = 1 "Auto indent off
	autocmd BufWritePost *sxhkdrc !killall sxhkd; setsid sxhkd &

	" save undo trees in files
	set undofile
	set undodir=~/.vim/undo/u
	
	" number of undo saved
	set undolevels=1000



" Copy, Paste
	set clipboard=unnamed
	nnoremap <C-t> :tabnew<cr>
	vnoremap <C-c> "*y :let @+=@*<CR>
	vnoremap <C-h> "*p :let @+=@*<CR>
	vnoremap <C-x> "*d :let @+=@*<CR>

" Latex
	map <leader>c :!compiler <c-r>%<CR>
	map <F3> :!wc <C-R>%<CR>
	map <F6> :!zathura <c-r>%<backspace><backspace><backspace>pdf &<CR><CR>
	map <F7> :setlocal spell! spelllang=de,cjk<CR>
	map <F9> :vsp<space>~/work/Latex/References.bib<CR>
	"map <Tab><Tab> <Esc>/<++><Enter>"_c4l
	"inoremap <Tab><Tab> <Esc>/<++><Enter>"_c4l
	"vnoremap <Tab><Tab> <Esc>/<++><Enter>"_c4l
	
	autocmd FileType tex,Rtex map <F3> :w !detex \| wc -w<CR>
	autocmd FileType tex,Rtex inoremap <F3> <Esc>:w !detex \| wc -w<CR>
	autocmd FileType tex,Rtex inoremap <F5> <Esc>:!xelatex<space><c-r>%<CR> 
	autocmd FileType tex,Rtex nnoremap <F5> :!xelatex<space><c-r>% <CR>

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))

" Plugin
	Plug 'kien/ctrlp.vim'
	Plug 'junegunn/fzf.vim'
	Plug 'scrooloose/nerdtree'
	Plug 'gko/vim-coloresque'
	Plug 'tpope/vim-surround'
	Plug 'itchyny/lightline.vim'
	Plug 'jiangmiao/auto-pairs' "Automatic quote and bracket completion
"	Plug 'pseewald/vim-anyfold'
"	Plug 'vim-syntastic/syntastic'
"	Plug 'lervag/vimtex'


" plugin test
	Plug 'svermeulen/vim-easyclip'
"	Plug 'tpope/vim-repeat'
	Plug 'tpope/vim-eunuch'
"	Plug 'mattn/emmet-vim'
	Plug 'terryma/vim-multiple-cursors'
	Plug 'prettier/vim-prettier'
	Plug 'junegunn/goyo.vim'
	Plug 'PotatoesMaster/i3-vim-syntax'
"	Plug 'ervandew/supertab'
	Plug 'vim-airline/vim-airline'
	Plug 'vim-scripts/vim-auto-save'
"	Plug 'dpelle/vim-LanguageTool'
	Plug 'sirver/ultisnips'
	Plug 'honza/vim-snippets'
	Plug 'tpope/vim-fugitive' "Irgendein Git plugin, kein Plan wasses macht

call plug#end()
"UltiSnips
  let g:UltiSnipsSnippetsDir = '~/.config/nvim/UltiSnips'
  let g:UltiSnipsEditSplit = 'vertical'
  let g:UltiSnipsExpandTrigger="<tab>"
  let g:UltiSnipsJumpForwardTrigger="<tab>"
  let g:UltiSnipsJumpBackwardTrigger="<s-tab>"


" Buffer durch :vsplit / :split erschaffen
" Buffer durch Strg-W + hjkl wechseln
" close buffer :bd



let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'

"let g:langtool_parameters = '--autoDetect'

" Plugin settings
	let g:highlightedyank_highlight_duration = 1500 	" set highlight duration time to 1000 ms, i.e., 1 second

	set statusline+=%#warningmsg#
	set statusline+=%{SyntasticStatuslineFlag()}
	set statusline+=%*
	"let g:languagetool_jar='/usr/bin/languagetool'
	let g:syntastic_always_populate_loc_list = 1
	let g:syntastic_auto_loc_list = 1
	let g:syntastic_check_on_open = 0
	let g:syntastic_check_on_wq = 1
	let g:auto_save = 1  " enable AutoSave on Vim startup
	let g:auto_save_in_insert_mode = 0  " do not save while in insert mode


nnoremap <leader>c :w<CR>:!rubber --pdf --warn all %<CR>
nnoremap <leader>v :!mupdf %:r.pdf &<CR><CR>
