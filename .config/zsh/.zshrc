export BROWSER="brave-browser"
export EDITOR="zsh"
export VISUAL="nvim"
export TERMINAL="st"
export EDITOR="nvim"



#setopt prompt_subst
#export FZF_DEFAULT_COMMAND="find -L"

source ~/.config/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

autoload -U colors && colors	# Load colors

antigen bundle git
antigen bundle pip
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle sudo
antigen bundle zsh-users/zsh-autosuggestions



antigen theme XsErG/zsh-themes themes/lazyuser
#antigen theme agnoster



# Tell Antigen that you're done.
antigen apply



# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char




# Some aliases
alias e="exit"
alias p="sudo pacman"
alias sp="sudo pacman -S"
alias sy="sudo pacman -Syu"
alias pq="pacman -Q"
alias sc="systemctl"
alias y="yay -S"
alias yy="yay -Syu"
alias x="chmod +x"
alias v="nvim"
alias sv="sudo nvim"
alias ka="killall"
alias gc="git clone"
alias trem="transmission-remote"
alias mkd="mkdir -pv"
alias ctimer="nohup ./.programs/blockkeeper-x86_64.AppImage &"
alias ref="shortcuts && source ~/.config/zsh/.zshrc" # Refresh shortcuts manually and reload bashrc
alias de="sdcv -u \"German - English\""
alias dg="sdcv -u \"English - German\""
alias Ds="sdcv -u \"English - Swedish\""
alias bw="wal -i ~/.config/wall.png" # Rerun pywal
alias pi="bash ~/.larbs/wizard/wizard.sh"
alias sa="sudo /opt/lampp/lampp start"
alias wt="curl wttr.in"
alias noisy="cd ~/.programms/noisy/ && python noisy.py --config config.json"
alias timeline="python3 ~/.programs/timeline/timeline-2.1.0/source/timeline.py"
alias neof="neofetch"
alias startx="startx "$XDG_CONFIG_HOME"/x11/xinitrc"
alias mpw="mpw -u 'Jasper Jansen'"

# Adding color
alias ls='ls -hN --color=auto --group-directories-first'
alias grep="grep --color=auto" # Color grep - highlight desired sequence.
alias ccat="highlight --out-format=ansi" # Color cat - print file with syntax highlighting.

source ~/.config/shortcuts/shortcuts
#source ~/.shortcuts
